//
//  CustomViewerWindow.swift
//  GlanceSDKDemo_iOS_Swift
//
//  Created by Ankit Desai on 4/30/18.
//  Copyright © 2018 Glance Networks. All rights reserved.
//

import UIKit

class CustomViewerWindow : UIWindow {
    var agentView : UIView?

    init(_ frame:CGRect, view:UIView, size:CGSize) {
        super.init(frame: frame)
        
        self.agentView = view
        self.agentView!.frame = CGRect(x: frame.size.width - 10.0 - size.width,
                                       y: frame.size.height - 10.0 - size.height,
                                       width: size.width,
                                       height: size.height)
        
         #if swift(<4.2)
            self.windowLevel = UIWindowLevelAlert
        #else
            self.windowLevel = UIWindow.Level.alert
        #endif
        self.rootViewController = UIViewController.init()
        self.rootViewController!.view.frame = frame
        self.rootViewController!.view.backgroundColor = UIColor.clear
        self.rootViewController!.view.addSubview(self.agentView!)
        
        let drag : UIPanGestureRecognizer  = UIPanGestureRecognizer(target: self, action: #selector(self.handleGlanceViewerDrag))
        self.agentView!.addGestureRecognizer(drag)
        self.agentView!.isUserInteractionEnabled = true
        
        self.isHidden = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if (self.agentView!.frame.contains(point)) {
            return self.agentView;
        }
        return nil;
    }
    
    @objc func handleGlanceViewerDrag(recognizer : UIPanGestureRecognizer) {
        let translation : CGPoint = recognizer.translation(in: self.rootViewController!.view)
        recognizer.view!.center = CGPoint(x: recognizer.view!.center.x + translation.x,
                                         y: recognizer.view!.center.y + translation.y)
        recognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.rootViewController!.view)
    }
}
